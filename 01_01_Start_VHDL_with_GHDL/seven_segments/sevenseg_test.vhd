library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity sevenseg_test is
end;

architecture sim of sevenseg_test is

	component sevenseg is
		port(
			S : in unsigned(3 downto 0);
			segments : out std_logic_vector(6 downto 0)
		);
	end component;
	
	signal S : unsigned(3 downto 0) := 4d"0";
	signal segments : std_logic_vector(6 downto 0);
	
begin
	
	dut : sevenseg port map(S, segments);
	
	process begin
		for i in 0 to 15 loop
			S <= to_unsigned(i,4);
			wait for 10 ns;
		end loop;
	end process;

end sim;
